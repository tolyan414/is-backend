require('dotenv').config()
const express = require('express')
require('log-timestamp')
const app = express()
const port = process.env.PORT || 3000 
const cors = require('cors')
const { getRates } = require('./use-cases/getRates')

app.use(cors())

app.get('/', async (req, res) => {
  const rates = await getRates()
  if (rates) {
    res.send(rates)
  } else {
    res.status(500).send({ message: 'Что-то пошло не так...' })
  }
})

app.listen(port, () => {
  console.log(`fs-backend listening at http://localhost:${port}`)
})
