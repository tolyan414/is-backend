module.exports = {
  testEnvironment: 'node',

  globalSetup: '<rootDir>/tests/testSetup.js',
  globalTeardown: '<rootDir>/tests/testTeardown.js'
}