const { Rates } = require('../models')
const { loadRates } = require('../helpers/loadRates')

async function getRates() {
  // берем документ из монги не старее CACHE_TIME минут
  const dt = new Date(new Date - process.env.CACHE_TIME * 60000)
  let rates = await Rates.findOne({ date: { $gte:  dt }}).sort({ _id: -1 })
  // если такого нет то берем данные из fixer.io и сохраняем в монгу
  if (!rates) {
    rates = await loadRates()
    if (rates) {
      await Rates.create(rates)
      await deleteOldRates()
    } else {
      rates = await getOldRates()
    }
  }
  return rates
}

async function getOldRates() {
  const rates = await Rates.findOne().sort({ _id: -1 })
  console.error(rates 
    ? `Возвращаем старые данные от ${rates.date}`
    : `Не получилось обработать запрос`
  )
  return rates
}

// храним курсы только за последние 30 дней
async function deleteOldRates() {
  const d = new Date()
  d.setDate(d.getDate() - 30)
  await Rates.deleteMany({ date: { $lt: d }})
}

module.exports = { getRates }
