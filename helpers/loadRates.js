const axios = require('axios')

async function loadRates() {
  const url = `http://data.fixer.io/api/latest?access_key=${process.env.FIXER_API_KEY}`
    + (process.env.SYMBOLS ? `&symbols=${process.env.SYMBOLS}` : '')
  try {  
    const rates = await axios.get(url)
    if (!rates.data.success) {
      console.log('error from fixer.io', rates.data)
    } else {
      console.log('Data from fixer.io loaded successfully')
      return prepareRates(rates.data)
    }
  } catch (e) {
    console.error('Error loading data from ', url, e)
  }  
  return false
}

function prepareRates(rates) {
  const rts = Object.keys(rates.rates).map(k => ({ 
    currency: k,
    rate: rates.rates[k] 
  }))
  return {
    base: rates.base,
    date: new Date(),
    rates: rts
  }
}

module.exports = { loadRates }