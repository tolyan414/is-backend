const { getRates } = require('./getRates')

test('getRates use case must return a valid doc', async () => {
  const ratesDoc = await getRates()
  expect(ratesDoc.rates.length).toBeGreaterThan(0)
})