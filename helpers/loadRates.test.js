const { loadRates } = require('./loadRates')

test('loadRates must load data from fixer.io', async () => {
  const ratesDoc = await loadRates()
  expect(ratesDoc.rates.length).toBeGreaterThan(0)
})