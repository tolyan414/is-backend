const { closeMongoConnection } = require('../models')

// not working :( mongoose connection after all tests still active
module.exports = function() {
  return closeMongoConnection().then(() => {
    console.log('Mongoose connection closed')
  })
}

