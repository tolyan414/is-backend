const mongoose = require('mongoose')

mongoose.connect(process.env.MONGO_URI, {
  useNewUrlParser: true, 
  useUnifiedTopology: true,
  useCreateIndex: true
})

const Rates = mongoose.model('Rates', {
  // в текущей реализации индекс на дату совсем не нужен,
  // так как сортировка по _id, но пускай будет для порядка ;)
  date: { type: Date, index: true },
  base: String,
  rates: [{
    currency: String,
    rate: Number
  }]
})

// for tests
function closeMongoConnection() {
  return mongoose.connection.close()
}

module.exports = { Rates, closeMongoConnection }