FROM node:current-alpine3.10

WORKDIR /src/

# todo: place all code into src folder and copy one folder 
COPY package*.json ./
COPY helpers ./helpers
COPY use-cases ./use-cases
COPY models.js ./
COPY app.js ./

RUN npm install

EXPOSE 5000

CMD node app.js
